<?php

spl_autoload_register(
    static function ($class_name) {
        require 'utils/' . $class_name . '.php';
    }
);


$nom = htmlspecialchars(ucfirst(strtolower($_POST['nom'])));
$race = htmlspecialchars(($_POST['race']));
$classe = htmlspecialchars($_POST['classe']);
$personnage = new $classe($nom, $race);
$personnage->raceBonus();
$pv = $personnage->getPV();
$dmg = $personnage->getDMG();
$ca = $personnage->getCA();
$arme = $personnage->getArme();
$sort = $personnage->getSort();

include 'header.php';
?>


<?php 
//     echo "nom: $nom <br>";
//      echo "pv: $pv <br>";
//      echo "classe d'armure: $ca <br>";
//      echo "classe : $classe <br>";
//      echo "race: $race <br>";
//      echo "arme: $arme <br>";
//      echo "dégâts: $dmg <br>";
//      echo "sort: $sort <br>";
// die;
?>



<main class="container">
    <h2>Voici votre personnage :</h2>
    <div class="fiche-perso">
        <div class="show-name">NOM: <span class="info"><?php echo $nom ?></span></div>
        <div class="show-pv"><span class="info"><?php echo $pv ?></span> PV</div>
        <div class="show-ca"><span class="info"><?php echo $ca ?></span> CA</div>
        <div class="show-classe">CLASSE: <span class="info"><?php echo $classe ?></span></div>
        <div class="show-race">RACE: <span class="info"><?php echo $race ?></span></div>
        <div class="show-weapon">Votre <span class="info"><?php echo $arme ?></span> inflige <span class="info"><?php echo $dmg ?></span> points de dégâts</div>
        <div class="show-spell">Vous connaissez le sort suivant: <span class="info"><?php echo $sort ?></span></div>
    </div>
</main>


<?php ?>
<?php
include 'footer.php';
?>