<?php
include 'header.php';
?>

<main class='container'>

    <form action="result.php" autocomplete="off" method="POST">

        <div class="name-field field">
        <label for="nom" class="form-label">Nom : </label>
            <input type="text" name="nom" id="nom" required> 
        </div>

        <div class="classe-field field">
        <label for="classe" class="form-label">Classe :</label>
        <select name="classe" id="classe" required>
                <option disabled selected value class="default-option"></option>
                <option value="Barbare">Barbare</option>
                <option value="Guerrier">Guerrier</option>
                <option value="Paladin">Paladin</option>
                <option value="Ranger">Ranger</option>
                <option value="Voleur">Voleur</option>
                <option value="Ingenieur">Ingenieur</option>
                <option value="Druide">Druide</option>
                <option value="Sorcier">Sorcier</option>
                <option value="Barde">Barde</option>
            </select>
        </div>

        <div class="race-field field">
        <label for="race" class="form-label">Race :</label> 
            <select name="race" id="race" required>
            <option disabled selected value class="default-option"></option>
                <option value="Humain">Humain</option>
                <option value="Halfelin">Halfelin</option>
                <option value="Nain">Nain</option>
                <option value="Elfe">Elfe</option>
                <option value="Orque">Orque</option>
                <option value="Golem">Golem</option>
            </select>
        </div>

        <div class="submit-container">
            <button type="submit" class="submit-btn" value="Valider">Valider</button>
        </div>

    </form>
    
</main>

<?php
include 'footer.php';