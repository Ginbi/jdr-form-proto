<?php

class Barde extends CA_Leger {

    use T_Races;

    public function __construct(string $nom, string $race)
    {
        parent::__construct($nom, $race);
        $this->arme = 'Rapière étincelante';
        $this->dmg = 20;
        $this->sort = 'Sérénade';
    }

}