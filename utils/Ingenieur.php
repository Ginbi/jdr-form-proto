<?php

class Ingenieur extends CA_Medium {

    use T_Races;

    public function __construct(string $nom, string $race)
    {
        parent::__construct($nom, $race);
        $this->arme = 'Bombarde';
        $this->dmg = 40;
        $this->sort = 'Peau de fer';
    }

}