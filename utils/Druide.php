<?php

class Druide extends CA_Leger {

    use T_Races;

    public function __construct(string $nom, string $race)
    {
        parent::__construct($nom, $race);
        $this->arme = 'Serpe en or';
        $this->dmg = 10;
        $this->sort = 'Esprit animal';
    }

}