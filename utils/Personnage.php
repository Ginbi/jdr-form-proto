<?php

class Personnage 
{

    protected int $pv;  // Points de Vie
    protected int $ca;  // Classe d'Armure
    protected int $dmg;  // Dégâts
    protected string $nom;
    protected string $race;
    protected string $arme;
    protected string $sort;

    public function __construct(string $nom, string $race)
    {
        $this->nom = $nom;
        $this->race = $race;
    }

    public function getName()
    {
        return $this->nom;
    }

    public function getRace()
    {
        return $this->race;
    }

        public function getPV()
    {
        return $this->pv;
    }

    public function getDMG()
    {
        return $this->dmg;
    }

    public function getCA()
    {
        return $this->ca;
    }
    public function getArme()
    {
        return $this->arme;
    }
    public function getSort()
    {
        return $this->sort;
    }

}