<?php

class Guerrier extends CA_Lourd {

    use T_Races;

    public function __construct(string $nom, string $race)
    {
        parent::__construct($nom, $race);
        $this->arme = 'Épée en acier';
        $this->dmg = 15;
        $this->sort = 'Appel aux armes';
    }
}