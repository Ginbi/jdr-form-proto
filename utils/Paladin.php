<?php

class Paladin extends CA_Lourd {

    use T_Races;

    public function __construct(string $nom, string $race)
    {
        parent::__construct($nom, $race);
        $this->arme = 'Lance en argent';
        $this->dmg = 20;
        $this->sort = 'Justice Divine';
    }
}