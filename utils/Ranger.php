<?php

class Ranger extends CA_Medium {

    use T_Races;

    public function __construct(string $nom, string $race)
    {
        parent::__construct($nom, $race);
        $this->arme = 'Arc de chasse';
        $this->dmg = 30;
        $this->sort = 'Tir de l\'Aigle';
    }

}