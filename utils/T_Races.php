<?php

trait T_Races
{
    // protected $races = array ([
    //                         "Humain" => 0,
    //                         "Halfelin" => 1,
    //                         "Nain" => 2,
    //                         "Elfe" => 3,
    //                         "Orque" => 4,
    //                         ]);

    public function raceBonus()
    {
    switch($this->race){
        case "Humain":
            $this->dmg += 5;
            break;
        case "Halfelin":
            $this-> pv += 5;
            break;
        case "Nain":
            $this->ca += 10;
            break;
        case "Elfe":
            $this->ca += 5;
            break;
        case "Orque":
            $this->dmg += 10;
            break;
        case "Golem":
            $this->pv += 10;
            break;

    }

    }

}