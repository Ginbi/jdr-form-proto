<?php

class CA_Medium extends Personnage{

    public function __construct(string $nom, string $race)
    {
        parent::__construct($nom, $race);
        $this->pv = 80;
        $this->ca = 20;
    }

}