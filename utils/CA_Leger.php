<?php

class CA_Leger extends Personnage{

    public function __construct(string $nom, string $race)
    {
        parent::__construct($nom, $race);
        $this->pv = 70;
        $this->ca = 10;
    }

}