<?php

class Sorcier extends CA_Leger {

    use T_Races;

    public function __construct(string $nom, string $race)
    {
        parent::__construct($nom, $race);
        $this->arme = 'Baton de sorcier';
        $this->dmg = 5;
        $this->sort = 'Boule de feu';
    }

}