<?php

class Voleur extends CA_Medium {

    use T_Races;

    public function __construct(string $nom, string $race)
    {
        parent::__construct($nom, $race);
        $this->arme = 'Dague aiguisée';
        $this->dmg = 20;
        $this->sort = 'Invisibilité';
    }

}