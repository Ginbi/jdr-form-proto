<?php

class Barbare extends CA_Lourd {

    use T_Races;

    public function __construct(string $nom, string $race)
    {
        parent::__construct($nom, $race);
        $this->arme = 'Hache de guerre';
        $this->dmg = 30;
        $this->sort = 'Rage du Barbare';
    }
}